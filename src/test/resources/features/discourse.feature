#language: pt
Funcionalidade: WEB

  @hotel_info
  Cenario: Automatizar os fluxos definidos no Discourse
    Dado acessar a página 'https://www.discourse.org'
    Quando clicar na opção Demo disponível no menu principal
    E fazer scroll até o final da página
    Então imprimir a descrição de todos os tópicos fechados
    E imprimir a quantidade de itens de cada categoria e dos que não possui categoria
    E imprimir o título do tópico que contém o maior número de views