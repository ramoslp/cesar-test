#language: pt
Funcionalidade: WEB 02

  @hotel_info
  Cenario: Deve imprimir informações do hotel
    Dado o site 'http://www.trivago.com.br'
    Quando digitar o valor 'Natal' no campo de busca
    E clicar no botão Pesquisar
    E selecionar a opção ordenar 'Somente distância'
    E no segundo item da lista de hotéis
    Então imprimir o nome do hotel
    E imprimir textualmente a quantidade de estrelas que ele possui
    E imprimir o nome do site da oferta
    E imprimir o valor do quarto
    Quando clicar na localização
    E clicar no link Visualizar todas as comodidades
    Então imprimir as Comodidades do quarto