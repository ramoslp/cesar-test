package org.cesar.test.common;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Set;

public abstract class BasePage {

    protected WebDriver driver;
    protected String windowHandleMain;

    private static final int LOAD_TIMEOUT = 30;

    protected void selectByVisibleText(WebElement element, String text) {
        aguardarElementoVisivel(element);
        new Select(element).selectByVisibleText(text);
    }

    protected void preencherCampo(WebElement element, CharSequence... keysToSend) {
        aguardarElementoVisivel(element);
        element.clear();
        element.sendKeys(keysToSend);
    }

    protected void click(WebElement element) {
        aguardarElementoVisivel(element);
        element.click();
    }

    protected String obterTexto(WebElement element) {
        aguardarElementoVisivel(element);
        return element.getText();
    }

    protected void aguardarElementoVisivelLocalizadoEm(By element) {
        new WebDriverWait(driver, LOAD_TIMEOUT).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(element));
    }

    protected void aguardarElementoVisivel(WebElement element) {
        new WebDriverWait(driver, LOAD_TIMEOUT).until(ExpectedConditions.visibilityOf(element));
    }

    protected void scrollUp() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("scroll(0, -250);");
    }

    protected void scrollDown() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
    }

    protected void switchWindow() {
        windowHandleMain = driver.getWindowHandle();
        Set<String> windowHandles = driver.getWindowHandles();
        for (String windowHandle : windowHandles) {
            if (!windowHandle.equals(windowHandleMain)) {
                driver.switchTo().window(windowHandle);
            }
        }
    }
}
