package org.cesar.test.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class DriverFactory {

    public static WebDriver getDriver() {
        String OS = System.getProperty("os.name");
        switch (OS) {
            case "Windows":
                System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
                return new ChromeDriver();
            case "Linux":
                System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");
                return new ChromeDriver();
            default:
                throw new RuntimeException("Unsupported Operational System: " + OS);
        }
    }
}
