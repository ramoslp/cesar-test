package org.cesar.test.steps;

import org.cesar.test.common.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public class TrivagoStepsSupport extends BasePage {

    protected WebElement secondElement;

    protected String hotelName;
    protected String hotelStarRating;
    protected String hotelPricePartner;
    protected String hotelPrice;
    protected String hotelAmenities;

    protected void closeCalendar() {
        click(driver.findElement(By.xpath("//div[@data-qa='calendar']//button[@class='df_overlay_close_wrap overlay__close']")));
    }

    protected String getStarRating(WebElement element) {
        try {
            return element.findElement(By.xpath(".//div[@class='quick-info']/div/meta")).getAttribute("content");
        } catch (NoSuchElementException e) {
            return "Não possui";
        }
    }
}
