package org.cesar.test.steps;

import org.cesar.test.common.BasePage;
import org.openqa.selenium.By;

public class DiscourseStepsSupport extends BasePage {

    protected void goToLatest() {
        click(driver.findElement(By.xpath("//a[text()='Latest']")));
        aguardarElementoVisivelLocalizadoEm(By.xpath("//th[text()='Views']"));
    }

    protected void goToCategories() {
        click(driver.findElement(By.xpath("//a[text()='Categories']")));
        aguardarElementoVisivelLocalizadoEm(By.id("categories-only-category"));
    }
}
