package org.cesar.test.steps;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import org.openqa.selenium.By;

import static org.cesar.test.common.DriverFactory.getDriver;

public class TrivagoSteps extends TrivagoStepsSupport {

    @Dado("o site {string}")
    public void oSite(String website) {
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");

        driver = getDriver();
        driver.get(website);
        driver.manage().window().maximize();
    }

    @Quando("digitar o valor {string} no campo de busca")
    public void digitarOValorNoCampoDeBusca(String destino) {
        preencherCampo(driver.findElement(By.id("querytext")), destino);
        aguardarElementoVisivel(driver.findElement(By.id("ssg-suggestions")));
    }

    @Quando("clicar no botão Pesquisar")
    public void clicarNoBotão() {
        click(driver.findElement(By.xpath("//button[@data-qa='search-button']")));
        closeCalendar();
    }

    @Quando("selecionar a opção ordenar {string}")
    public void selecionarAOpçãoOrdenar(String sortBy) {
        selectByVisibleText(driver.findElement(By.xpath("//select[@data-qa='sorting']")), sortBy);
    }

    @Quando("no segundo item da lista de hotéis")
    public void noSegundoItemDaListaDeHoteis() {
        By secondElementLocator = By.xpath("//section/ol/li[position()=2]");
        aguardarElementoVisivelLocalizadoEm(secondElementLocator);
        secondElement = driver.findElement(secondElementLocator);
    }

    @Então("imprimir o nome do hotel")
    public void imprimirONomeDoHotel() {
        hotelName = obterTexto(secondElement.findElement(By.xpath(".//span[@data-qa='item-name']")));
    }

    @Então("imprimir textualmente a quantidade de estrelas que ele possui")
    public void imprimirTextualmenteAQuantidadeDeEstrelasQueElePossui() {
        hotelStarRating = getStarRating(secondElement);
    }

    @Então("imprimir o nome do site da oferta")
    public void imprimirONomeDoSiteDaOferta() {
        hotelPricePartner = obterTexto(secondElement.findElement(By.xpath(".//span[@data-qa='recommended-price-partner']")));
    }

    @Então("imprimir o valor do quarto")
    public void imprimirOValorDoQuarto() {
        hotelPrice = obterTexto(secondElement.findElement(By.xpath(".//strong[@data-qa='recommended-price']")));
    }

    @Quando("clicar na localização")
    public void clicarNaLocalização() {
        click(secondElement.findElement(By.xpath(".//div[@data-qa='item-location-details']")));
    }

    @Quando("clicar no link Visualizar todas as comodidades")
    public void clicarNoLinkVisualizarTodasComodidades() {
        /* TODO: Not implemented. Access is denied.
        click(secondElement.findElement(By.xpath(".//span[text()='Visualizar todas as comodidades']")));
         */
    }

    @Então("imprimir as Comodidades do quarto")
    public void imprimirAsComodidadesDoQuarto() {
        System.out.println("Informações do hotel desejado");
        System.out.println("Nome: " + hotelName + " Estrelas: " + hotelStarRating);
        System.out.println("Oferta da empresa: " + hotelPricePartner + " Preço: " + hotelPrice);
        System.out.println("Comodidades do quarto: ");
    }
}
