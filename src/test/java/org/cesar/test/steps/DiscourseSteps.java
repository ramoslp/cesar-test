package org.cesar.test.steps;

import io.cucumber.java.After;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import java.util.List;

import static org.cesar.test.common.DriverFactory.getDriver;

public class DiscourseSteps extends DiscourseStepsSupport {

    @Dado("acessar a página {string}")
    public void acessarAPágina(String website) {
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");
        driver = getDriver();

        driver.get(website);
        driver.manage().window().maximize();
    }

    @Quando("clicar na opção Demo disponível no menu principal")
    public void clicarNaOpçãoDemoDisponívelNoMenuPrincipal() {
        click(driver.findElement(By.xpath("//a[text()='Demo']")));
        switchWindow();
        aguardarElementoVisivel(driver.findElement(By.xpath("//*[@class='text-logo' and text()='Demo']")));
    }

    @Quando("fazer scroll até o final da página")
    public void fazerScrollAtéOFinalDaPágina() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        Long scrollHeight = (Long) js.executeScript("return document.body.scrollHeight");
        while (true) {
            scrollDown();
            try {
                aguardarElementoVisivelLocalizadoEm(By.xpath("//h3[text()='There are no more latest topics.']"));
                scrollDown();
                break;
            } catch (NoSuchElementException ignored) {
            }
            Long newScrollHeight = (Long) js.executeScript("return document.body.scrollHeight");
            if (newScrollHeight.equals(scrollHeight)) {
                break;
            }
            scrollHeight = newScrollHeight;
        }
    }

    @Então("imprimir a descrição de todos os tópicos fechados")
    public void imprimirADescriçãoDeTodosOsTópicosFechados() {
        scrollUp();

        List<WebElement> trElements = driver.findElements(By.xpath("//tbody/tr"));
        for (int i = 1; i <= trElements.size(); i++) {
            try {
                WebElement lock = driver.findElement(By.xpath("//tbody/tr[" + i + "]//span[@title='This topic is closed; it no longer accepts new replies']"));
                if (lock.isDisplayed()) {
                    System.out.println(obterTexto(driver.findElement(By.xpath("//tbody/tr[" + i + "]//a[@class='title raw-link raw-topic-link']"))));
                }
            } catch (NoSuchElementException ignored) {
            }
        }
    }

    @Então("imprimir a quantidade de itens de cada categoria e dos que não possui categoria")
    public void imprimirAQuantidadeDeItensDeCadaCategoriaEDosQueNãoPossuiCategoria() {
        goToCategories();

        List<WebElement> trElements = driver.findElements(By.xpath("//tbody/tr"));
        for (int i = 1; i <= trElements.size(); i++) {
            String categoryName = obterTexto(driver.findElement(By.xpath("//tbody/tr[" + i + "]//span[@class='category-name']")));
            String numberOfTopics = obterTexto(driver.findElement(By.xpath("//tbody/tr[" + i + "]//span[@class='value']")));
            System.out.println(categoryName + "(" + numberOfTopics + ")");
        }
    }

    @Então("imprimir o título do tópico que contém o maior número de views")
    public void imprimirOTítuloDoTópicoQueContémOMaiorNúmeroDeViews() {
        goToLatest();
        click(driver.findElement(By.xpath("//th[text()='Views']")));
        aguardarElementoVisivelLocalizadoEm(By.xpath("//tbody/tr[1]"));
        System.out.println(obterTexto(driver.findElement(By.xpath("//tbody/tr[1]//a[@class='title raw-link raw-topic-link']"))));
    }

    @After
    public void tearDown() {
        if(driver != null) {
            driver.quit();
            driver = null;
        }
    }
}
