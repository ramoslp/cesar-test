package org.cesar.test.runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features",
        glue = "org.cesar.test.steps",
        plugin = {"pretty", "html:target/report-html"},
        snippets = CucumberOptions.SnippetType.CAMELCASE
)
public class TestRunner {
}
